# Ghidra Gitlab Group Auth

This is a very simple program that allows authenticating ghidra users based on gitlab group membership.
Right now it requires using a personal access token with `read_api` level permissions in the `password` field
in ghidra, and your supplied `username` must match your gitlab `username`.

Ghidra's implementation of jaas auth doesn't seem to allow deriving the username from the application,
which is why you must also enter your `username`.

