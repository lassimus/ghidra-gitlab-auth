package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type User struct {
	Id       int
	Username string
	Message  string
}

type Member struct {
	User
	State string
}

func main() {
	log.SetFlags(0)

	if len(os.Args) != 3 {
		log.Fatalf("Usage: %s <gitlab_url> <group_id>", os.Args[0])
	}

	gitlab_url := os.Args[1]
	group_id, err := strconv.Atoi(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Gitlab url: %s, Group id: %d", gitlab_url, group_id)

	reader := bufio.NewReader(os.Stdin)

	name, _ := reader.ReadString('\n')
	name = strings.TrimSuffix(name, "\n")

	access_token, _ := reader.ReadString('\n')
	access_token = strings.TrimSuffix(access_token, "\n")

	//log.Printf("Username: %s Private-Token: %s", name, access_token)

	user_url := fmt.Sprintf("%s/api/v4/user", gitlab_url)

	http_client := http.Client{
		Timeout: time.Second * 2,
	}
	req, err := http.NewRequest(http.MethodGet, user_url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Private-Token", access_token)

	res, err := http_client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	user := User{}
	if err = json.Unmarshal(body, &user); err != nil {
		log.Fatal(err)
	}

	if user.Message != "" {
		log.Fatalf("API error message: %s", user.Message)
	}

	if user.Username != name {
		log.Fatalf("Provided username: %s does not match returned username from gitlab: %s", name, user.Username)
	}

	//log.Println(user)

	member_url := fmt.Sprintf("%s/api/v4/groups/%d/members/%d", gitlab_url, group_id, user.Id)

	req, err = http.NewRequest(http.MethodGet, member_url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Private-Token", access_token)

	res, err = http_client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	member := Member{}
	if err = json.Unmarshal(body, &member); err != nil {
		log.Fatal(err)
	}

	if member.Message != "" {
		log.Fatalf("API error message: %s", member.Message)
	}

	//	log.Println(member)
	if member.State != "active" {
		log.Fatalf("User member state: %s", member.State)
	}

	log.Printf("Log in successful for user: %s", name)
}
